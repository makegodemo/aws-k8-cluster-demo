# Prerequisites
1. A working python 3 install with [pip](https://pip.pypa.io/en/stable/installing/)
2. IAM credentials for a user with access to an AWS account, either:
   - Exported as AWS_ACCESS_KEY_ID/AWS_SECRET_ACCESS_KEY
   - Saved in ~/.aws/credentials (under `default`)
3. [Accept ToS for Centos 7 in the AWS account](https://aws.amazon.com/marketplace/pp?sku=cvugziknvmxgqna9noibqnnsy)
   We have to accept that agreement before we can deploy CentOS images.
   This only needs to be done once per AWS account.

# Set Config
Review/update the AWS infrastructure config (defines VPC CIDR, subnets, instance sizes/count, etc):
- vars/aws_infrastructure.yml

Define some ENV vars we'll need and ensure all prerequisites to run the playbooks are installed

```
export AWS_DEFAULT_REGION=us-east-1 #Or the region of your choice
```

Set/Generate new password for Vault:
We're going to encrypt sensitive information for this deployment. One can either set the key to something of your liking,
or run the following and it'll generate a random key for you. Either way, don't lose this key as there will be no way
to access the encrypted data if you lose it (including being unable to SSH to the K8 instances).

```
export VAULT_PASSWORD=$(openssl rand -base64 20)
echo -e "\n== SAVE THIS VAULT PASSWORD ==\n${VAULT_PASSWORD}\n==============================\n" && \
read -p "Press enter to continue..."
```

# Local Init
Install Python prerequisites, including Ansible:
TODO: Use virtualenv to avoid issues with OS-installed Python versions

```
#Newer versions of Ansible need the cryptography module which will fail to compile if your PIP is not new enough
/usr/bin/env python3 -m pip install --update pip

#Install PIP modules (will also install Ansible)
/usr/bin/env python3 -m pip install -r requirements.txt
```

Install Ansible collections that we'll be using

```
ansible-galaxy install -r ansible-requirements.yml
```

# Deploy AWS VPC and Infrastructure
When ready, run that playbook to deploy the infrastructure with:

```
ansible-playbook tasks/create_aws_infra.yml
```

After that runs, you'll have a provisioned AWS infrastructure ready for K8.
There will also be a new (encrypted) SSH key placed in keys/. Theoretically one would commit back to the repo with:

```
git add keys/
git commit -m "Adding newly created SSH key"
git push
```

# Deploy K8 Cluster
```
#Load encrypted SSH key (so Ansible can connect to the instances to provision them)
ansible-vault view --vault-password-file scripts/vault_password keys/k8s-key.pem | ssh-add -

#Deploy with kubespray (Is this cheating? 😬). It's going to take about 5-10 minutes so now is a good time to grab a snack.
ansible-playbook -i inventories/aws_k8_dynamic.py kubespray/cluster.yml --user=centos --become --become-user=root
```

After that completes, the K8 cluster will be deployed. You could SSH to the instances and run `kubectl` with `sudo`
locally if desired, although that's not required.

# Connect GitLab to the new K8 cluster
In order to control/deploy to this K8 cluster with GitLab you'll need to enter the K8 service account information
into GitLab. The necessary information has been saved locally for you and you can view it with:

```
cat kubespray/artifacts/gitlab-service-account-data.txt
```

One would probably want to setup the `gitlab-runner` K8 executor to run builds in K8 as well. I've included a template of a good working `config.toml`
in `contrib/gitlab-runner/config.toml`. One would still need to set the K8 `url` and `bearer_token`, and could use the same information
that was used for GitLab, although really the runner should have it's own service account with least privilege.

TODO: Create dedicated service account(s) for gitlab-runner

Once GitLab/GitLab Runner are configured one could either delete the `gitlab-service-account-data.txt` or encrypt it and commit it back:

```
ansible-vault encrypt --vault-password-file scripts/vault_password kubespray/artifacts/gitlab-service-account-data.txt
git add kubespray/artifacts/gitlab-service-account-data.txt
git commit -m "Adding gitlab-service-account artifact"
git push
```

# Final steps
1. Remember to store the `VAULT_PASSWORD` somewhere safe. #lastchance
2. The K8 cluster deployment doesn't have an ingress controller (something is preventing that from deploying the NLB automatically).
   For now that is a manual process until I figure out what's wrong with that part of the config.

# Scaling the cluster
One can update the `vars/aws_infrastructure.yml` and re-run the two playbooks above and it'll expand the infrasture and add the
new controllers/nodes to the cluster.

Scaling down is possible as well although there is some cleanup that needs to be added to remove some unnecessary cluster resources.

# Other improvements
TODO: Remove all the unused cruft from kubespray (since this is only targeting AWS)
