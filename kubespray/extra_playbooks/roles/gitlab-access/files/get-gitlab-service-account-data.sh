#!/bin/sh

export PATH=$PATH:/usr/local/bin

DEFAULT_TOKEN=$(/usr/local/bin/kubectl get secrets | grep '^default-token' | awk '{print $1}')
CA=$(kubectl get secret ${DEFAULT_TOKEN} -o jsonpath="{['data']['ca\.crt']}" | base64 --decode)
GITLAB_SECRET=$(kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep gitlab | awk '{print $1}') | grep '^token:' | awk '{print $2}')

echo "CA:"
echo "${CA}"
echo
echo "SERVICE TOKEN: ${GITLAB_SECRET}"
